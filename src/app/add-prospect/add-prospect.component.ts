import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProspectModel, ProspectModelBuilder } from '../shared/prospecto.models';
import Swal from 'sweetalert2';
import { ProspectService } from '../shared/prospect.service';

@Component({
  selector: 'app-add-prospect',
  templateUrl: './add-prospect.component.html',
  styleUrls: ['./add-prospect.component.css']
})
export class AddProspectComponent implements OnInit {
  prospect:ProspectModel;
  labelFile = 'Buscar documento';
  multipleImages = [];
  constructor(private routeNavigate: Router, private prospectService: ProspectService) { 
    this.prospect = new ProspectModelBuilder().build();
  }

  ngOnInit(): void {
  }


  async saveProspect(prospect: ProspectModel){
    /*
    valores obligatorios nombre, primer apellido, calle, numero
    colonia, cpodigo postal, teléfono, rfc
    documento
    */
    if (this.prospect.nombre == ''){
      Swal.fire('Atención debe de capturar el nombre', 'warning');
      return;
    }
    if (this.prospect.primer_apellido == ''){
      Swal.fire('Atención debe de capturar el primer apellido', 'warning');
      return;
    }
    if (this.prospect.calle == ''){
      Swal.fire('Atención debe de capturar la calle', 'warning');
      return;
    }
    if (this.prospect.numero_casa == ''){
      Swal.fire('Atención debe de capturar el número de casa', 'warning');
      return;
    }
    if (this.prospect.colonia == ''){
      Swal.fire('Atención debe de capturar la colonia', 'warning');
      return;
    }
    if (this.prospect.codigo_postal == ''){
      Swal.fire('Atención debe de capturar el código postal', 'warning');
      return;
    }
    if (this.prospect.rfc == ''){
      Swal.fire('Atención debe de capturar el RFC', 'warning');
      return;
    }
    if (this.prospect.telefono == '0'){
      Swal.fire('Atención debe de capturar el telefono', 'warning');
      return;
    }
    this.prospectService
      .addProspect(prospect)
      .subscribe((response) => {
        Swal.fire('Exito', 'El Prospecto ha sido guardado' , 'success');
        this.routeNavigate.navigate(["prospectos"]);
      }, (error) => {
        if (!error.error.status_code) {
          Swal.fire(`Error ${error.error.code}`, error.error.message, 'error');
        } else {
          Swal.fire('Atención', error.error.message, 'warning');
        }
      }
    );
  }

  onMultipleFileSelect(event: any){
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
      this.labelFile = '' + event.target.files.length + ' Seleccionados.'
    } else {
      Swal.fire(
        'Aviso',
        'Un archivo minimo es requerido para alta.',
        'warning'
      );
    }
  }

  redirectToProspectList() {
    Swal.fire({
      title: 'Al salir se borrarán las datos capturados, ¿Desea salir?',
      icon: 'question',
      showDenyButton: true,
      confirmButtonText: `Si`,
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) {
        //this.resetFormValues();
        this.routeNavigate.navigate(["prospectos"]);
      }
    })
  }

  resetFields(){
    this.labelFile = '';
  }

  resetFormValues(){
    this.labelFile = 'Buscar documento';
    //this.file = '';
    this.prospect = new ProspectModelBuilder().build();
    //this.prospect.status = ApiContants.WAIT_STATUS;
    this.multipleImages = [];
  }


  /*
  async saveImageOnserver(pkProspect:number){
    try {

      if (this.multipleImages.length > 0) {
        await this.prospectService.sendFilesToAPI(
          this.formData,
          String(pkProspect)
          ).toPromise();
        
          Swal.fire({
            title: 'Guardado',
            text: "Deseas continuar creando otro prospecto?",
            icon: 'success',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Capturar otro.',
            cancelButtonText: 'Regresar a lista general.'
          }).then((result) => {
            if (result.isConfirmed) {
              this.resetFormValues();
            }else{
              this.resetFormValues();
              this.routeNavigate.navigate(["/inicio/prospect/list"]);
            }
          });

      }else{
        Swal.fire(
          'Aviso',
          'Un archivo minimo es requerido para alta.',
          'warning'
        );
        
      }
    } catch (e) {
      Swal.fire(
        'Error',
        'Ha ocurrido un error desconocido.',
        'error'
      );
    }
  }
  */
}
